package com.example.helloworld;

import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import static org.junit.Assert.assertThat;

public class MainActivityViewModelTest {

    @Mock
    MainActivityViewModel SUT;

    @Before
    public void setUp() {
        SUT= new MainActivityViewModel();
        SUT.setUp();
    }

    @Test
    public void  testValue_true() {
        boolean expected = true;
        SUT.mEmployeeId.set("Hello");
        boolean actual ;
        if(SUT.mEmployeeId.get().equals("Hello")){
            actual=true;
        }else{
            actual=false;
        }
        assertThat(expected, CoreMatchers.is(actual));
    }


    @Test
    public void  testValue_false() {
        boolean expected = false;
        SUT.mEmployeeId.set("ABCD");
        boolean actual ;
        if(SUT.mEmployeeId.get().equals("Hello")){
            actual=true;
        }else{
            actual=false;
        }
        assertThat(expected, CoreMatchers.is(actual));

    }
}
