package com.example.helloworld;

import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableField;

public class MainActivityViewModel extends BaseViewModel {
    public ObservableField<String> mEmployeeId = new ObservableField<>();

    public void setUp() {
        mEmployeeId.set("Hello");
    }

    public void onEmpIdTextChanged(CharSequence empIdText) {
        mEmployeeId.set(empIdText.toString());
        System.out.println("text::" + mEmployeeId.get());
    }
 }
