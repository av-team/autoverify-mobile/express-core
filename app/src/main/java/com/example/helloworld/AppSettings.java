package com.example.helloworld;

import android.content.Context;

public final class AppSettings {
    private static Context context;

     public static Context getContext() {
        return context;
    }

    public static void setContext(Context context) {
        AppSettings.context = context;
    }

}
