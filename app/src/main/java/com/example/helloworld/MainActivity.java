package com.example.helloworld;

import android.os.Bundle;
import android.widget.EditText;

import com.example.helloworld.databinding.MainActivityDataBinding;

public class MainActivity extends BaseActivity<MainActivityDataBinding,MainActivityViewModel> {

    public static EditText et;
    public MainActivityDataBinding mainActivityBinding;
    private MainActivityViewModel viewModel = new MainActivityViewModel();
    ViewModelProviderFactory factory;

    @Override
    public int getBindingVariable() {
        return BR.mainViewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public MainActivityViewModel getViewModel() {
        viewModel= ViewModelProviders.of(this, factory).get(MainActivityViewModel.class);
        return viewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainActivityBinding = getViewDataBinding();
        mainActivityBinding.setMainViewModel(viewModel);
        mainActivityBinding.textInputEmployeeId.setText("Hello");
       // et= findViewById(R.id.textInputEmployeeId);
       // getText();
        viewModel.setUp();

    }


}
