package com.example.helloworld;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;

public class BaseViewModel extends ViewModel {
    public Context context = AppSettings.getContext();


    public MutableLiveData<Void> mDisplayProgress = new MutableLiveData<>();
    public final MutableLiveData<Void> mDismissProgress = new MutableLiveData<>();

    /**
     * Mutable live data for display progress dialog
     *
     * @return - Mutable object for progress dialog to display
     */
    public MutableLiveData<Void> getmDisplayProgress() {
        return mDisplayProgress;
    }

    /**
     * Mutable live data for dismiss progress dialog
     *
     * @return - Mutable object for progress dialog to dismiss
     */
    public MutableLiveData<Void> getmDismissProgress() {
        return mDismissProgress;
    }
}
